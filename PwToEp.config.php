<?php
$config = array(
  'apiKey' => array(
    'type' => 'text',
    'label' => 'API key',
    'description' => "Etherpad API key", 
    'pattern' => '[a-z0-9-/]+',
    'minlength' => 1,
    'required' => true, 
    'value' => 'api'
  ),
  'epURL' => array(
    'type' => 'URL',
    'label' => 'Instance URL',
    'description' => "Etherpad instance URL", 
    
    'minlength' => 1,
    'required' => true, 
    'value' => 'http://instance.url'
  )
);
?>