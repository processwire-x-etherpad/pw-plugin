<?php namespace ProcessWire;

require_once wire('config')->paths->siteModules . "PwToEp/vendor/autoload.php";

class EpInterface{
    public function __construct($apiKey, $epURL) {
        
        $this->instance = new \EtherpadLite\Client($apiKey, $epURL . '/api/');
        
    }

    public function createPad($padId){
        $this->instance->createPad($padId);
    }

    public function padExists($padId){
        $allPads = $this->instance->listAllPads();
        return(in_array($padId, $allPads->padIDs));
    }

    public function getReadOnlyPadId($padId){
        $obj = $this->instance->getReadOnlyID($padId);
        return $obj->readOnlyID;
    }

    #################PROCESSWIRE TO ETHERPAD

    //cleanHTMLForEP: prepare HTML from PW to Etherpad.
    public function cleanHTMLForEP($html){
        
        //replace all the <p></p> by double <br>. This is ugly but it is what Etherpad needs.
        $html = \str_replace('</p>', '<br><br>', $html);
        $html = \str_replace('<p>', '', $html);
       
        return $html;
    }

    public function makeEPTag($fieldName, $lng, $subFieldName = null, $ind = null){
        if($subFieldName != null && $lng != null){
            $subFieldName .= '_'. $lng;
        }else if($lng != null){
            $fieldName .= '_'. $lng;
        }
        $openTag = array('___', $fieldName);
        if($subFieldName != null){
            array_push($openTag, '--', $ind, '--', $subFieldName);

        }
        $openTag[] = '___';

        $closeTag = array_merge(array(), $openTag);
        array_splice($closeTag, 1, 0, '/');
        return array('open' => implode('', $openTag),
                     'close' => implode('', $closeTag));
            
    }
    public function makeEPElement($page, $field, $lng = null, $fieldParent = null, $ind = null){
        
        //FIRST WE PROCESS THE CONTENT
        //check if this is a language field. If so iterative call for every lng
        //echo($field->type);

        if($field->type instanceof FieldtypeLanguageInterface){
           
            if($lng == null){
                $languages = $page->getLanguages();
                
                $output = '';
                $i = 0;
                foreach ($languages as $lang) {
                    $lang = $lang->name;
                    $output .= $this->makeEPElement($page, $field, $lang, $fieldParent, $ind);
                    if($i < count($languages) -1)
                        $output .= '<br><br>';

                    $i++;
                }
                return $output;
            }else{
                $field->langBlankInherit = LanguagesPageFieldValue::langBlankInheritNone;
                $fieldContent = $page->getLanguageValue($lng, $field->name);
                
                if($field->type == 'FieldtypeTextareaLanguage')
                    $fieldContent = $this->cleanHTMLForEP($fieldContent);
                
            }
        
        }else if($field->type == 'FieldtypeTextarea'){
            $fieldContent = $this->cleanHTMLForEP($page->get($field->name));
        }else if($field->type == 'FieldtypePage'){//the pages are inserted as paths
            $linkedPages = [];
            foreach($page->get($field->name) as $linkedPage){
                $linkedPages[] = $linkedPage->path;
            };
            $fieldContent = \implode('|', $linkedPages);
        
        }else if($field->type == 'FieldtypeDatetime'){
            $fieldContent = $page->get($field->name);
            if($fieldContent != '') {
                $fieldContent = $page->getUnformatted($field->name);
                $fieldContent =  date('Y-m-d H:i:s',$fieldContent);
            }
        }else if($field->type == 'FieldtypeImage'){
            $page->of(false);
            $fieldContent = '';
            $languages = $page->getLanguages();
            foreach($page->get($field->name) as $image){
                $imgResized = $image->width(500);
                $fieldContent .= wire('config')->urls->httpRoot . $imgResized->url;
                
                foreach ($languages as $lang) {
                    $fieldContent .= '<br>'.$image->description($lang->name);
                    
                }
                
            }
        }else{
            $fieldContent = $page->get($field->name);
        }
        //THEN WE GET THE TAG
        if($fieldParent != null)
            $tag = $this->makeEPTag($fieldParent->name, $lng, $field->name, $ind);
        else
            $tag = $this->makeEPTag($field->name, $lng);

        //AND WE BUILD THE ELEMENT
        $output = $tag['open'].'<br>'.$fieldContent.'<br>'.$tag['close'];
        
        return $output;
        
    }

    
    public function saveToPad($page, $padId){
        //those are the fieldsTypes that we can process and send to EP
        $validTypes = array('FieldtypePageTitleLanguage', 'FieldtypeTextareaLanguage', 'FieldtypeTextLanguage',
                            'FieldtypeText', 'FieldtypePage', 'FieldtypeURL', 'FieldtypeDatetime', 
                            'FieldtypeRepeater', 'FieldtypeCheckbox', 'FieldtypeImage');

        
       
        $fields = $page->fields;
        $languages = $page->getLanguages();

        $output = '';
        
        foreach ($fields as $key => $field) {
            
            //first we check if we can process the field
            if(!in_array($field->type, $validTypes))
                continue;

            $fieldName = $field->name;
            
            //repeater field!
            if($field->type == 'FieldtypeRepeater'){
                
                //first we get the current repeater (pageArray) from the repeater field
                $subPages = $page->get($fieldName);
                
                //for each subPage of the repeater
                for($subPageI = 0; $subPageI < $subPages->count(); $subPageI++){
                    $subPage = $subPages->get($subPageI);
                    $repeaterFields = $subPage->fields;
                    foreach($repeaterFields as $repeaterField){
                        $output .= $this->makeEPElement($subPage, $repeaterField, null, $field, $subPageI);
                        $output .= '<br><br>';
                    }

                }
               

            }else{
                $output .= $this->makeEPElement($page, $field, null);
                $output .= '<br><br>';
            }
        }
        //echo $output;
        
        $this->instance->setHTML($padId, $output);
        
    }
    
    #################ETHERPAD TO PROCESSWIRE

    //this one is tricky: we try to get a clean and usable HTML from Etherpad.
    public function cleanHTMLForPW($html){
        //first we make <p> from <br><br>
        $paragraphs = explode("<br><br>", $html);
        
        if(count($paragraphs) > 1)
        {
            for ($i = 0; $i < count($paragraphs); $i++)
            {
                if($paragraphs[$i] != "" AND $paragraphs != "&nbsp;")
                {
                    $paragraphs[$i] = '<p>' . $paragraphs[$i] . '</p>';
                }
            }
        
            $html = implode ('', $paragraphs);
        }
        
        //then we try to get a better html from what etherpad gives us
        $purifier = wire('sanitizer')->purifier();
        $purifier->set('AutoFormat.AutoParagraph', true);
        $purifier->set('AutoFormat.RemoveEmpty', true);
        $purifier->set('AutoFormat.RemoveEmpty.RemoveNbsp', true);
        $html = $purifier->purify($html);

        //remove br's from those kind of things: <br><br></p>, <p><br /><br>, etc.
        $html = preg_replace( "#((<p>)\s?\K(<br\s?\/?>\s?)+)|(<br\s?\/?>\s?)+(?=\s?<\/p>)#", '', $html);
        //then, remove <p></p>
        $html = preg_replace("#<p>\s?<\/p>\s?#", '', $html);
        

        //we also have to remove footnotes links generated by etherpad
        $html = preg_replace("#<sup><a.*?href=.*?>\*<\/a><\/sup>#", "", $html);
        
        return $html;

    }

    //analyze etherpad tag to extract fieldname, language and subfieldname in case of repeater
    public function parseFieldName($fieldName){
        $output = array('name' => $fieldName, 'lng' => null);
    
        //check for lng field
        $fieldNameParts = explode('_', $fieldName);
        if(count($fieldNameParts) >= 2){
            $output['name'] = $fieldNameParts[0];
            $output['lng'] = $fieldNameParts[1];
        }
        //check for repeater (and thus subfieldname)
        $fieldNameParts = explode('--', $fieldName);
        if(count($fieldNameParts) >= 3){
            //this is the name of the repeater field
            $output['name'] = $fieldNameParts[0];
            //this is the id of the repeater page
            $output['subId'] = $fieldNameParts[1];
            //this is the name of the field inside the repeater page
            $output['subname'] = $fieldNameParts[2];

        }
        return $output;
    }
    public function setFieldValue($page, $fieldName, $value, $lng){
        
        //get field object from $fieldName
        $field = wire('fields')->get($fieldName);
        //prepare value depending of fieldType
        
        switch($field->type){
            
            case 'FieldtypePage':
                $value = html_entity_decode(strip_tags($value));
                $value = preg_replace( "/\r|\n/", "", $value);

                $linkedPagePaths = explode('|', $value);
                //the value of a FieldtypePage is a PageArray
                $value = PageArray();
                foreach($linkedPagePaths as $linkedPagePath){
                    $linkedPagePath = strip_tags($linkedPagePath);
                    $linkedPage = wire('pages')->get("path=".$linkedPagePath);
                    if($linkedPage->id)
                        $value->add($linkedPage);
                }        
                break;
            case 'FieldtypeTextareaLanguage':
                $value = $this->cleanHTMLForPW($value);
                break;
            case 'FieldtypeDatetime':
                if($value != '')
                    $value = wire('datetime')->stringToTimestamp($value, 'Y-m-d H:i:s');
                break;
            case 'FieldtypeImage':
                //do not import anything related to images 
                return;
            default:
                $value = html_entity_decode(strip_tags($value));
                $value = preg_replace( "/\r|\n/", "", $value);

        }
        if($lng != 'none')
            $page->setLanguageValue($lng, $fieldName, $value);
        else
            $page->set($fieldName, $value);
    }

    public function saveToPage($page, $padId){
        //first get and parse html content
        $page->of(false);
        $content = $this->instance->getHTML($padId);
        $htmlContent = $content->html;
        preg_match_all('#___(.*)___(.*)___(/.*)___#iU', $htmlContent, $matches);
       
        //loop through fields
        foreach($matches[0] as $key => $item){
            $value = $matches[2][$key];

            $fieldName = $matches[1][$key];
            $fieldInfos = $this->parseFieldName($fieldName);
            
            $field = wire('fields')->get($fieldName);

            //repeater field!
            if(isset($fieldInfos['subId'])){
                //first we get the current repeater (pageArray) from the repeater field
                $subPages = $page->get($fieldInfos['name']);
                //then we get the the subPage we need to modify. If it doesn't exists, create one
                $subPage = $subPages->get($fieldInfos['subId']);
                
                if(!$subPage->id)
                    $subPage = $subPages->getNew();
                
                //then we set the subField value to the subPage
                $subPage->of(false);
                $this->setFieldValue($subPage, $fieldInfos['subname'], $value, $fieldInfos['lng']);
                $subPage->save();

            }else{
                $page->of(false);
                $this->setFieldValue($page, $fieldInfos['name'], $value, $fieldInfos['lng']);
                $page->save();
            }
            
           
        }
        
        
        

    }

}

?>