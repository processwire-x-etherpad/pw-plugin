let padSaved = false;

function saveToPW(pageId){
    let ajaxURL = ProcessWire.config.urls.admin + 'page/';
    ajaxURL = ajaxURL + 
						"?id=" + pageId + 
                        "&render=JSON&start=0&limit=0&do=savetopw";
    $.getJSON(ajaxURL, function(data) {
        console.log(data);
    });
}
function confirmAndOverwrite(e){
    let doOverwrite = confirm('Warning: this will overwrite all changes and comments made to the Pad since its last save!');
    e.stopImmediatePropagation();

    if(!doOverwrite){
        e.preventDefault();
        return false;
    }
}
function hookToEditPage(){
    
    $('#Inputfield_overwrite_pad').on('click', confirmAndOverwrite);
}

function hookToPWListing(){
    
    $('.PageListActionEditPad a, .PageListActionReadPad a').off('click');
    $('.PageListActionEditPad a, .PageListActionReadPad a').click(function(e){

        e.preventDefault();

        let match = $(this).attr('href').match('[?&]' + 'id' + '=([^&]+)');
        let pageId = match ? match[1] : null;
        let options = {};
        console.log($(this).parents('.PageListActionEditPad').length);
        if($(this).parents('.PageListActionEditPad').length > 0){
            console.log('set button');
            options = {'buttons':[
                {
                    text: "Save to Processwire",
                    icon: "ui-icon-heart",
                    click: function() {
                        let doSave = confirm('Warning: this will overwrite all changes made to the text fields in Processwire!');
                        if(doSave)
                            saveToPW(pageId);
                    }
            
                
                }
            ]
            }
        }
        

        let win =  pwModalWindow($(this).attr('href'), options);
        

        $(document).on('pw-modal-closed', function(e){
            e.preventDefault();
            padSaved = false;
            if(pageId != null){
                $(document).trigger('pageListRefresh', pageId);
            }
            
        });
       
        return false;
    });
}

(function($, config) {
    console.log('script loaded ok'); 
    $(document).on('ready', function(){
        hookToPWListing()
        hookToEditPage(); 
    });
    $(document).on('pageListRefresh', hookToPWListing);
    $(document).ajaxComplete(hookToPWListing);
    


})(jQuery, config); 